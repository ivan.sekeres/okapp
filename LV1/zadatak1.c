#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int moreEvenOrOdd(int* P, int size) {
    int ctEven = 0;
    int ctOdd = 0;
    int i;

    for (i = 0; i < size; i++) {
        if (P[i] % 2 == 0) {
            ctEven++;
        } 
        else if (P[i] % 2 == 1) {
            ctOdd++;
        }

        if (ctEven > ctOdd) {
            return 2;
        } 
        else if (ctEven < ctOdd)  {
            return 1;
        }
    }
    
    return 0;
}

int main(void) {
    int i;
    int n;
    int A[10];

    printf("Enter the array size between 1 and 10:");

    do {
        scanf("%d", &n);
        if (n > 10 || n < 1) {
            printf("Wrong input!\n");
        }
    } while (n < 1 || n > 10);

    for (i = 0; i < n; i++) {
        printf("Enter %d. array element:", i + 1);
        scanf("%d", &A[i]);
    }

    if (moreEvenOrOdd(A, n) == 2) {
        printf("There are more even than odd numbers.");
    } 
    else if(moreEvenOrOdd(A, n) == 1) {
        printf("There are more odd than even numbers.");
    }
    else {
        printf("Equal.");
    }

    return 0;
}