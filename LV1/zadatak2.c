#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int lastDigit(int number);

int countDigits(int number);

int absVal(int number);

int main(void) {
    int number;
    int functionNumber;

    printf("Enter integer number: ");
    scanf("%d", &number);

    printf("Enter function number: \n");
    printf("1 Last Digit \n");
    printf("2 Count digits \n");
    printf("3 Absolute value \n");
    scanf("%d", &functionNumber);

    switch (functionNumber)
    {
    case 1:
        printf("%d", lastDigit(number));
        break;
    case 2:
        printf("%d", countDigits(number));
        break;
    case 3:
        printf("%d", absVal(number));
        break;
    default:
        break;
    }
}

int lastDigit(int number) {
    return number % 10;
}

int countDigits(int number) {
    int count = 0;
    do {
        number /= 10;
        count++;
    } while (number != 0);

    return count;
}

int absVal(int number) {
    if (number < 0) {
        return number * -1;
    } 
    else if (number > 0) {
        return number;
    } 
    
    return 0;
}