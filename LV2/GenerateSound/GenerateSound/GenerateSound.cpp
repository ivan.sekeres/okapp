#include <sndfile.hh>
#include <iostream>
#include <math.h>
using namespace std;

int main()
{
    const int format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;
    //  const int format=SF_FORMAT_WAV | SF_FORMAT_FLOAT;
    const int channels = 2;
    const int sampleRate = 48000;
    const char* outfilename = "foo.wav";

    cout << "wav_writer..." << endl;

    SndfileHandle outfile(outfilename, SFM_WRITE, format, channels, sampleRate);
    if (!outfile) return -1;

    // prepare a 5 seconds buffer and write it
    const int size = sampleRate * 2;
    float sample[size], factor = 1.0;
    for (int i = 0; i < size/2; i=i+2) {
        sample[i] = sin(float(i) / size * 3.14 * 1000) * factor;
    }
	for (int i = size/2+1; i < size; i = i + 2) {
		sample[i] = sin(float(i) / size * 3.14 * 1000) * factor;
	}

    outfile.write(&sample[0], size);

    cout << "done!" << endl;
    return 0;
}
