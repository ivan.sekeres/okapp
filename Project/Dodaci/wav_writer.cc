/*
 * wav_writer.cc
 *
 *      Author: ec
 *      wav_writer.cc
 *      tool to generate wav-files
 *      see: http://parumi.wordpress.com/2007/12/16/how-to-write-wav-files-in-c-using-libsndfile/
 *
 */



#include "/usr/include/sndfile.hh"
#include <iostream>
#include <cmath>
using namespace std;

int main()
{
	const int format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;   // defining a file format
	const int channels = 1;	// defining a number of channels
	const int sampleRate = 48000;	// defining a sample rate
	const char* outfilename = "name_of_the_file.wav";	// defining a new file name

	cout << "wav_writer..." << endl;	// optional message for the user

	//	preparing new file for writing
	SndfileHandle outfile(outfilename, SFM_WRITE, format, channels, sampleRate);
	if (not outfile) return -1;

	const int size = sampleRate * 5;	// prepare a 5 seconds buffer
	float sample[size], factor = 1.0;	// prepare an array to write values

	// writing values into array of specific size
	for (int i = 0; i < size; i++) {
		sample[i] = sin(float(i) / size * M_PI * 3000) * factor;
	}

	outfile.write(&sample[0], size);	// writing values from array to file

	cout << "done!" << endl;	// optional message for the user
	return 0;
}

