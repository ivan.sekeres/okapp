#include <stdlib.h>
#include <stdio.h>
#include "/usr/include/GL/glut.h"
#include "/usr/include/GL/gl.h"
#include "/usr/include/sndfile.hh"
#include <math.h>
#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <cstdlib>
#include <thread>
#include <chrono>

using namespace std;
void display(void);
void reshape(int, int);
void idle();
void readSensors(unsigned char, int, int);
void draw(void);
void play(const char* file_name, int miliseconds);
int sensors_state[4] = {0, 0, 0, 0};
bool direction = 0;
const char* R1 = "zvukovi/R-1.wav";
const char* R2 = "zvukovi/R-2.wav";
const char* L1 = "zvukovi/L-1.wav";
const char* L2 = "zvukovi/L-2.wav";
const char* LR1 = "zvukovi/LR-1.wav";
const char* LR2 = "zvukovi/LR-2.wav";

unsigned char* loadPPM(const char* filename, int& width, int& height) {
	const int BUFSIZE = 128;
	FILE* fp;
	unsigned int read;
	unsigned char* rawData;
	char buf[3][BUFSIZE];
	char* retval_fgets;
	size_t retval_sscanf;
	if ((fp = fopen(filename, "rb")) == NULL)
	{
		std::cerr << "error reading ppm file, could not locate " << filename << std::endl;
		width = 0;
		height = 0;
		return NULL;
	}
	retval_fgets = fgets(buf[0], BUFSIZE, fp);
	do
	{
		retval_fgets = fgets(buf[0], BUFSIZE, fp);
	} while (buf[0][0] == '#');
	retval_sscanf = sscanf(buf[0], "%s %s", buf[1], buf[2]);
	width = atoi(buf[1]);
	height = atoi(buf[2]);
	do
	{
		retval_fgets = fgets(buf[0], BUFSIZE, fp);
	} while (buf[0][0] == '#');
	rawData = new unsigned char[width * height * 3];
	read = fread(rawData, width * height * 3, 1, fp);
	fclose(fp);
	if (read != 1)
	{
		std::cerr << "error parsing ppm file, incomplete data" << std::endl;
		delete[] rawData;
		width = 0;
		height = 0;
		return NULL;
	}
	return rawData;
}

void initGL()
{
	glEnable(GL_TEXTURE_2D); // enable texture mapping
	glShadeModel(GL_SMOOTH); // enable smooth shading
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // get clear background (black color)
	glClearDepth(1.0f); // color depth buffer
	glDepthFunc(GL_LEQUAL); // configuration of depth testing
							//enable additional options regarding: perspective correction, anti-aliasing, etc
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
}

void loadTexture()
{
	GLuint texture[1]; // declaring space for one texture
	int twidth, theight; // declaring variable for width and height of an image
	unsigned char* tdata; // declaring pixel data
						  // loading image data from specific file:
	tdata = loadPPM("auto3.ppm", twidth, theight);
	if (tdata == NULL) return; // check if image data is loaded
							   // generating a texture to show the image
	glGenTextures(1, &texture[0]);
	glBindTexture(GL_TEXTURE_2D, texture[0]);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, twidth, theight, 0, GL_RGB, GL_UNSIGNED_BYTE,
		tdata);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

int main(int argc, char** argv) {
	/* 1) INITIALIZATION */
	// initialize GLUT
	glutInit(&argc, argv);
	// set window position and size
	glutInitWindowPosition(545, 180);
	glutInitWindowSize(720, 720);
	// set the combination of predefined values for display mode
	// set color space (Red, Green, Blue - RGB)
	// alocate depth buffer
	// set the size of the buffer (double)
	glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
	// create window
	glutCreateWindow("Screen");
	/* 2) REGISTRATION OF CALLBACK FUNCTION */
	// function called when new window need to be drawn
	glutDisplayFunc(display);
	// function called when window changes the size
	glutReshapeFunc(reshape);
	// function called when nothing else is executing and CPU is free
	glutIdleFunc(idle);
	// function called when keyboard key is pressed
	glutKeyboardFunc(readSensors); // custom function 'readSensors' can	be implemented separately
	loadTexture();
	initGL();
	/* 3) START GLUT PROCESSING CYCLE */
	glutMainLoop();
	return 0;
}

void readSensors(unsigned char key, int x, int y) {
	switch (key) {
//  sensor front right
	case 'q':
		sensors_state[0] = 0;
		draw();
		break;
	case 'w':
		sensors_state[0] = 1;
		draw();
		break;
	case 'e':
		sensors_state[0] = 2;
		draw();
		break;
	case 'r':
		sensors_state[0] = 3;
		draw();
		break;
//  sensor front left
	case 'a':
		sensors_state[1] = 0;
		draw();
		break;
	case 's':
		sensors_state[1] = 1;
		draw();
		break;
	case 'd':
		sensors_state[1] = 2;
		draw();
		break;
	case 'f':
		sensors_state[1] = 3;
		draw();
		break;
//  sensor back right
	case 't':
		sensors_state[2] = 0;
		draw();
		break;
	case 'y':
		sensors_state[2] = 1;
		draw();
		break;
	case 'u':
		sensors_state[2] = 2;
		draw();
		break;
	case 'i':
		sensors_state[2] = 3;
		draw();
		break;
//  sensor back left
	case 'g':
		sensors_state[3] = 0;
		draw();
		break;
	case 'h':
		sensors_state[3] = 1;
		draw();
		break;
	case 'j':
		sensors_state[3] = 2;
		draw();
		break;
	case 'k':
		sensors_state[3] = 3;
		draw();
		break;
// car direction and reset states
	case 'z':
		direction = 0;
		break;
	case 'x':
		direction = 1;
		break;
	case 'c':
		sensors_state[0] = 0;
		sensors_state[1] = 0;
		sensors_state[2] = 0;
		sensors_state[3] = 0;
		display();
		break;
	}
}

void display() {
	cerr << "display callback" << endl;
	// clean color buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	// start drawing quads
	glBegin(GL_QUADS);
	// choose color (white)
	glColor3f(1, 1, 1);
	// coordinates of initial white rectangle for the background
	glTexCoord2f(0, 1); glVertex3f(-2, -1, 0);
	glTexCoord2f(1, 1); glVertex3f(2, -1, 0);
	glTexCoord2f(1, 0); glVertex3f(2, 1, 0);
	glTexCoord2f(0, 0); glVertex3f(-2, 1, 0);
	// end of drawing
	glEnd();
	// swap buffers to show new graphics
	glutSwapBuffers();
}

void reshape(int width, int height)
{
	cerr << "reshape callback" << endl;
	// specify the desired rectangle
	glViewport(0, 0, width, height);
	// switch to matrix projection
	glMatrixMode(GL_PROJECTION);
	// clean projection matrix
	glLoadIdentity();
	// set camera view (orthographic projection with 4x4 unit square canvas)
	glOrtho(-2, 2, -2, 2, 2, -2);
	// swith back to matrix
	glMatrixMode(GL_MODELVIEW);
}

void idle()
{
	if(sensors_state[0] > sensors_state[1] && !direction)
	{
		if(sensors_state[0] == 1)
			play(R1, 1000);
		else if(sensors_state[0] == 2)
			play(R1, 500);
		else if(sensors_state[0] == 3)
			play(R1, 200);
	}
	else if(sensors_state[0] < sensors_state[1] && !direction)
	{
		if(sensors_state[1] == 1)
			play(L1, 1000);
		else if(sensors_state[1] == 2)
			play(L1, 500);
		else if(sensors_state[1] == 3)
			play(L1, 200);
	}
	else if(sensors_state[0] == sensors_state[1] && !direction)
	{
		if(sensors_state[1] == 1)
			play(LR1, 1000);
		else if(sensors_state[1] == 2)
			play(LR1, 500);
		else if(sensors_state[1] == 3)
			play(LR1, 200);
	}
	else if(sensors_state[2] > sensors_state[3] && direction)
	{
		if(sensors_state[2] == 1)
			play(R2, 1000);
		else if(sensors_state[2] == 2)
			play(R2, 500);
		else if(sensors_state[2] == 3)
			play(R2, 200);
	}
	else if(sensors_state[2] < sensors_state[3] && direction)
	{
		if(sensors_state[3] == 1)
			play(L2, 1000);
		else if(sensors_state[3] == 2)
			play(L2, 500);
		else if(sensors_state[3] == 3)
			play(L2, 200);
	}
	else if(sensors_state[2] == sensors_state[3] && direction)
	{
		if(sensors_state[3] == 1)
			play(LR2, 1000);
		else if(sensors_state[3] == 2)
			play(LR2, 500);
		else if(sensors_state[3] == 3)
			play(LR2, 200);
	}
}

void play(const char* file_name, int miliseconds)
{
	string command1 = "aplay ";
	string command = command1 + file_name;
	system(command.c_str());
	std::this_thread::sleep_for(std::chrono::milliseconds(miliseconds));
}

void draw()
{
	float sensor_draw_values[4][3][8] = {
		{
			{-1.48, 0.21, -1.4, 0.16, -1.14, 0.55, -1.22, 0.6},
			{-1.35, 0.2, -1.28, 0.16, -1.09, 0.46, -1.15, 0.5},
			{-1.22, 0.2, -1.15, 0.16, -1.03, 0.36, -1.1, 0.4}
		},{
			{-1.48, -0.21, -1.4, -0.16, -1.14, -0.55, -1.22, -0.6},
			{-1.35, -0.2, -1.28, -0.16, -1.09, -0.46, -1.15, -0.5},
			{-1.22, -0.2, -1.15, -0.16, -1.03, -0.36, -1.1, -0.4}
		},{
			{1.48, 0.21, 1.4, 0.16, 1.14, 0.55, 1.22, 0.6},
			{1.35, 0.2, 1.28, 0.16, 1.09, 0.46, 1.15, 0.5},
			{1.22, 0.2, 1.15, 0.16, 1.03, 0.36, 1.1, 0.4}
		},{
			{1.48, -0.21, 1.4, -0.16, 1.14, -0.55, 1.22, -0.6},
			{1.35, -0.2, 1.28, -0.16, 1.09, -0.46, 1.15, -0.5},
			{1.22, -0.2, 1.15, -0.16, 1.03, -0.36, 1.1, -0.4}
		}
	};

	float colors[3][3] =
	{
		{1.0, 0.50, 0.50},
		{1.0, 0.35, 0.35},
		{1.0, 0.0, 0.0}
	};

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glBegin(GL_QUADS);
	glColor3f(1, 1, 1);
	glTexCoord2f(0, 1); glVertex3f(-2, -1, 0);
	glTexCoord2f(1, 1); glVertex3f(2, -1, 0);
	glTexCoord2f(1, 0); glVertex3f(2, 1, 0);
	glTexCoord2f(0, 0); glVertex3f(-2, 1, 0);
	glEnd();

	glBegin(GL_QUADS);
	for(int i = 0; i < (int)sizeof(sensors_state); i++)
	{
		for(int j = 0; j < sensors_state[i]; j++)
		{
			glColor3f(colors[j][0], colors[j][1], colors[j][2]);
			glVertex3f(sensor_draw_values[i][j][0], sensor_draw_values[i][j][1], 0.0f);
			glVertex3f(sensor_draw_values[i][j][2], sensor_draw_values[i][j][3], 0.0f);
			glVertex3f(sensor_draw_values[i][j][4], sensor_draw_values[i][j][5], 0.0f);
			glVertex3f(sensor_draw_values[i][j][6], sensor_draw_values[i][j][7], 0.0f);
		}
	}

	glEnd();
	glutSwapBuffers();
}
